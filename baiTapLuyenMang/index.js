var n = [];

function themSo() {

    var nhapSoN = document.getElementById("txt-nhap-so-n").value * 1;
    n.push(nhapSoN);
    var dsPhanTu = "";
    for (var i = 0; i < n.length; i++) {
        dsPhanTu = dsPhanTu.concat(n[i], ", ");
    }
    document.getElementById("ket-qua").innerHTML = dsPhanTu;
    document.getElementById("txt-nhap-so-n").value = "";

}

// 1. Tổng số dương
function tongSoDuong() {

    var tinhTongSoDuong = 0;
    for (var i = 0; i < n.length; i++) {
        if (n[i] > 0) {
            tinhTongSoDuong = tinhTongSoDuong + n[i];
        }
    }
    document.getElementById("ket-qua1").innerHTML = "Tổng số dương: " + tinhTongSoDuong;
}

// 2. Đếm số dương
function demSoDuong() {

    var demSoDuong = 0;
    for (var i = 0; i < n.length; i++) {
        if (n[i] > 0) {
            demSoDuong++;
        }
    }
    document.getElementById("ket-qua2").innerHTML = "Đếm số dương: " + demSoDuong;
}

// 3. Tìm số nhỏ nhất
function timSoNhoNhat() {

    var soNhoNhat = n[0];
    for (var i = 1; i < n.length; i++) {
        if (n[i] < n[0]) {
            soNhoNhat = n[i];
        }
    }
    document.getElementById("ket-qua3").innerHTML = "Số nhỏ nhất: " + soNhoNhat;
}

//4. Tìm số dương nhỏ nhất
function timSoDuongNhoNhat() {
    var temp = [];

    for (var i = 0; i < n.length; i++) {
        if (n[i] > 0) {
            temp.push(n[i]);
        }
    }

    var soDuongNhoNhat = temp[0];

    for (var j = 1; j < temp.length; j++) {
        if (temp[j] < soDuongNhoNhat) {
            soDuongNhoNhat = temp[j];
        }
    }

    document.getElementById("ket-qua4").innerHTML = "Số dương nhỏ nhất: " + soDuongNhoNhat;
};

//5. Tìm số chẵn cuối cùng
function timSoChanCuoiCung() {

    var soChanCuoi = "";

    for (var i = 0; i < n.length; i++) {
        if (n[i] % 2 == 0) {
            soChanCuoi = n[i];
        }
    }

    document.getElementById("ket-qua5").innerHTML = "Số chẵn cuối cùng: " + soChanCuoi;
};

// 6. Đổi chỗ

function doiCho() {
    var tempArr = [];
    for (var i = 0; i < n.length; i++) {
        tempArr.push(n[i])
    };

    var tempValue = 0;
    var viTri1 = document.getElementById("txt-vi-tri-1").value * 1;
    var viTri2 = document.getElementById("txt-vi-tri-2").value * 1;

    tempValue = tempArr[viTri1];
    tempArr[viTri1] = tempArr[viTri2];
    tempArr[viTri2] = tempValue;

    var ketQua = "";
    for (var j = 0; j < tempArr.length; j++) {
        ketQua = ketQua.concat(tempArr[j], ", ");
    }

    document.getElementById("ket-qua6").innerHTML = "Mảng sau khi đổi chỗ: " + ketQua;
};

// 7. Sắp xếp tăng dần
function sapXepTangDan() {
    var sapXepTangDan = "";
    n.sort();
    for (var i = 0; i < n.length; i++) {
        sapXepTangDan = sapXepTangDan.concat(n[i], ", ");
    }
    document.getElementById("ket-qua7").innerHTML = "Mảng sau khi sắp xếp: " + sapXepTangDan;

};

// 8. Tìm số nguyên tố đầu tiên
function soNguyenToDauTien() {
    var soNguyenToDauTien = "";
    var laSNT = true;
    for (var i = 0; i < n.length; i++) {
        laSNT = true;
        for (var j = 2; j < n[i]; j++)
            if (n[i] % j == 0) {
                laSNT = false;
                break;
            }
        if (laSNT == true) {
            soNguyenToDauTien = n[i];
            break;
        }
    }
    document.getElementById("ket-qua8").innerHTML = "Số nguyên tố đâu tiên trong mảng: " + soNguyenToDauTien;

};

// 9. Đếm số nguyên
var mangSoThuc = [];

function themSoThuc() {
    var nhapSoN = document.getElementById("txt-nhap-so").value * 1;
    mangSoThuc.push(nhapSoN);
    var dsPhanTu = "";
    for (var i = 0; i < mangSoThuc.length; i++) {
        dsPhanTu = dsPhanTu.concat(mangSoThuc[i], ", ");
    }
    document.getElementById("ket-qua-9").innerHTML = dsPhanTu;
    document.getElementById("txt-nhap-so").value = "";
}

function demSoNguyen() {
    var demSoNguyen = 0;
    for (var i = 0; i < mangSoThuc.length; i++) {
        if (Number.isInteger(mangSoThuc[i])) {
            demSoNguyen++;
        }
    }
    document.getElementById("ket-qua-9-1").innerHTML = "Số nguyên trong mảng: " + demSoNguyen;

};
// 10. So sánh số lượng số âm và dương
function SoSanhSoAmDuong() {
    var SLsoAm = 0;
    var SLsoDuong = 0;
    var ketQua = "";

    for (var i = 0; i < n.length; i++) {
        if (n[i] < 0) {
            SLsoAm++;
        } else if (n[i] > 0) {
            SLsoDuong++;
        }
    }
    if (SLsoAm < SLsoDuong) {
        ketQua = "Số âm < Số dương";
    } else if (SLsoAm > SLsoDuong) {
        ketQua = "Số âm > Số dương";
    } else {
        ketQua = "Số âm = Số dương";
    }


    document.getElementById("ket-qua-10").innerHTML = ketQua;
}